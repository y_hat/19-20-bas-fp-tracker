/*
    @author: vfaner
    @description: Pull bas_fp_roster_19_20_STAGING table for Tracker updates
*/

SELECT *

FROM ODS_CPS_STAGING.DAT.bas_fp_roster_19_20_STAGING
